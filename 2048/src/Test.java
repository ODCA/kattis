import java.util.Scanner;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Test {

    public static void main (String[] args) {
        new Test().run();
    }

    public void run() {

        Scanner sc = new Scanner(System.in);
        String[][] board = new String[4][4];

        for (int i = 0; i < 4; i++) {
            board[i] = sc.nextLine().split(" ");
        }

        int direction = Integer.parseInt(sc.nextLine());
        System.err.println("direction: " + direction);

        //move tiles to the left
        if (direction == 0) {
            for (int i = 0; i < board.length; i++) {
                board[i] = moveLeft(board[i]);
            }
        }

        else if (direction == 1) {
            for(int i = 0; i < board.length; i++) {

                String[] column = new String[4];
                for(int j = 0; j < board.length; j++) {
                    column[j] = board[j][i];
                }

                // for (String s : column) System.err.print(s + " ");
                // System.err.println();
                column = moveLeft(column);

                for(int j = 0; j < board.length; j++) {
                    board[j][i] = column[j];
                }
            }
        }

        else if (direction == 2) {
            for (int i = 0; i < board.length; i++) {

                board[i] = reverseStringArray(board[i]);
                board[i] = moveLeft(board[i]);
                board[i] = reverseStringArray(board[i]);
            }
        }

        else if (direction == 3) {

            for(int i = 0; i < board.length; i++) {

                String[] column = new String[4];
                for(int j = 0; j < board.length; j++) {
                    column[j] = board[j][i];
                }

                column = reverseStringArray(column);
                column = moveLeft(column);
                column = reverseStringArray(column);

                for(int j = 0; j < board.length; j++) {
                    board[j][i] = column[j];
                }
            }

        }

        for (String[] line : board) {
            System.out.println(line[0] + " " + line[1] + " " + line[2] + " " + line[3]);
        }
    }

    private String[] reverseStringArray(String[] array) {
        for (int i = 0; i < array.length/2; i++) {
            String temp = array[i];
            array[i] = array[array.length -i -1];
            array[array.length -i -1] = temp;
        }

        return array;
    }

    private String[] moveLeft(String[] line) {

        for (int i = 0; i < line.length; i++) {
            for ( int j = i + 1; j < line.length; j++) {

                if(line[i].equals("0")) {
                    line[i] = line[j];
                    line[j] = "0";
                }

                else if (line[i].equals(line[j])) {
                    line[i] = Integer.toString(Integer.parseInt(line[i]) + Integer.parseInt(line[j]));
                    line[j] = "0";
                    j = line.length;
                }

                else if(Integer.parseInt(line[j]) != 0) {
                    j = line.length;
                }
            }
        }
        return line;
    }



}
