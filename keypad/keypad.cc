#include <iostream>
#include <bitset>
#include <chrono>

#define MAX_ROWS 10
#define MAX_COLS 10

void print(bool impossible, char output[MAX_ROWS][MAX_COLS], int rows, int cols) {
    if(impossible) {
        std::cout << "impossible\n----------\n";
        return;
    }

    for(int r = 0; r < rows; r++) 
    {
        for (int c = 0; c < cols; c++)
        {
            std::cout << output[r][c];
        }
        std::cout << "\n";
    }
    std::cout << "----------\n";

}

void function() 
{
    int nbr_tests;
    std::cin >> nbr_tests;
    int rows, cols;

    

    for(int t = 0; t < nbr_tests; t++) 
    {
        std::bitset<MAX_COLS> keypadcols[MAX_ROWS];
        std::bitset<MAX_ROWS> keypadrows[MAX_COLS];
        char output[MAX_ROWS][MAX_COLS];
        bool imposible = false;

        std::cin >> rows >> cols;
        
        for(int r = 0; r < rows; r++) 
        { 
            std::string row;
            std::cin >> row;
            for(int c = 0; c < cols; c++) { 
                keypadrows[r][c] = row[c] - '0';
                keypadcols[c][r] = row[c] - '0';
            }
        }

        if (rows == 1) 
        {
            for(int c = 0; c < cols; c++) {
                if (keypadrows[0].test(c)) 
                    output[0][c] = 'P';
                else
                    output[0][c] = 'N';
            }
        }
        
        else if (cols == 1) 
        { 
            for(int r = 0; r < rows; r++) {
                if (keypadcols[0].test(r)) 
                    output[r][0] = 'P';
                else
                    output[r][0] = 'N';
            }
        }

        else 
        {
            int r = 0;
            while(r < rows && !imposible)
            {
                int c = 0;
                while(c < cols && !imposible)
                {
                    if(keypadrows[r].test(c)) 
                    {
                        // if(keypadrows[r].count() == 1 && keypadcols[c].count() == 1) 
                        // {
                        //     imposible = true;
                        // }
                        
                        if(keypadrows[r].count() == 1 || keypadcols[c].count() == 1) {
                            output[r][c] = 'P';
                            
                        }
                    
                        else 
                            output[r][c] = 'I';

                    } else 
                    {
                        if(keypadrows[r].count() > 0 && keypadcols[c].count() > 0) 
                        {
                            imposible = true;
                        }

                        output[r][c] = 'N';
                    }
                    c++; 
                } 
                r++;
            }
        }
        print(imposible, output, rows, cols);
    }
}

int main(int argc, char const *argv[])
{
    // auto t1 = std::chrono::high_resolution_clock::now();
    function();
    // auto t2 = std::chrono::high_resolution_clock::now();
    // auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();

    // std::cout << duration << "\n";
    
    
    /* code */
    return 0;
}
