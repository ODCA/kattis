#include <iostream>
#include <unistd.h>

void time(int time) {
    
    sleep(time);

}

int main(int argc, char const *argv[])
{
    time(5);
    //hur gick det att utföra detta programmet?
    std::cout << "Bra" << std::endl;
    time(4);
    // Fanns det något du kunde göra bättre?
    std::cout << "Nej" << std::endl;
    time(4);
    //varför då?
    std::cout << "För att jag bara lyder dina instruktioner" << std::endl;
    time(6);
    //Använde du projektcirkeln
    std::cout << "Nope" << std::endl;
    time(5);
    //varför då?s
    std::cout << "Du gav mig inte möjligheten att planera själv" << std::endl;
    time(7);
    //kommer du använda den i framtiden?
    std::cout << "Beror på dig" << std::endl;
    time(7);
    //Lärde du dig något när du utförde uppgiften
    std::cout << "Inte denna gången" << std::endl;
    //varför då?
    time(5);
    std::cout << "Du sparade inte resultatet åt mig" << std::endl;
    //Jag tycker inte att vi kommer så mycket längre här
    time(8);
    std::cout << "Låter bra" << std::endl;
    //har du något bra förslag? 
    time(4);
    std::cout << "Rymdraket" << std::endl;
    time(2);

    return 0;
}
