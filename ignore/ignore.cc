#include <iostream>
#include <string>
#include <algorithm>

// print the next number valid number we will se on the dispaly.
// 0 1 2 5 6 8 9 are valid numbers,
// note!! 6 -> 9 && 9 -> 6
// ingore numbers containing 3, 4, 7
// example the 98th valid number we will see is 200 but upside down it becomes 002 


int main(int argc, char const *argv[])
{
    std::string input;
    while (std::getline(std::cin, input))
    {
        
        int k_ans = std::stoi(input);
        // std::cout << "calculating " << k_ans << "s number\n";
        std::string i = "0";
        int k = 0;
        std::size_t found;

        while (k < k_ans) {
            i = std::to_string(std::stoi(i) + 1);

            if (i.find("3") != std::string::npos ) {
                // std::cout << "found 3, i = " << i << "\n";
            }
            else if (i.find("4") != std::string::npos ) {
                // std::cout << "found 4, i = " << i << "\n";
                //4 exists
            }
            else if (i.find("7") != std::string::npos ) {
                // std::cout << "found 7, i = " << i << "\n";
                //7 exists
            }
            else {
                k++;
            }
        }

        std::reverse(i.begin(), i.end());

        std::string out = "";
        for(char& c : i) {
            if(c == '6') 
                out += '9';
            else if(c == '9') 
                out += '6';
            else 
                out += c;
        }

        std::cout << out << "\n";
    }
    
    return 0;
}

