import java.lang.StringBuffer;
import java.util.HashSet;
import java.util.Scanner;

public class Karte {

    public static void main (String[] args) {
        new Karte().run();
    }

    public void run() {
        Scanner sc = new Scanner(System.in);
        StringBuffer line = new StringBuffer(sc.nextLine());

        HashSet<Integer> h = new HashSet<Integer>();
        HashSet<Integer> k = new HashSet<Integer>();
        HashSet<Integer> t = new HashSet<Integer>();
        HashSet<Integer> p = new HashSet<Integer>();

        Boolean greska = false;


        if (line.length() % 3 != 0) {
            System.err.println("String is not devideable with three");
        }

        else {
            while (line.length() > 0 && !greska) {
                String card = line.substring(0,3);
                System.err.println(card + ", " + line);
                line = line.delete(0, 3);

                if (card.charAt(0) == 'H') {
                    if (!h.add(Integer.parseInt(card.substring(1,3)))) {
                        greska = true;
                        line = new StringBuffer();
                    }
                }
                else if (card.charAt(0) == 'K') {
                    if (!k.add(Integer.parseInt(card.substring(1,3)))) {
                        greska = true;
                        line = new StringBuffer();
                    }
                }
                else if (card.charAt(0) == 'T') {
                    if (!t.add(Integer.parseInt(card.substring(1,3)))) {
                        greska = true;
                        line = new StringBuffer();
                    }
                }
                else {
                    if (!p.add(Integer.parseInt(card.substring(1,3)))) {
                        greska = true;
                        line = new StringBuffer();
                    }
                }
            }

            if (!greska) System.out.println((13 - p.size()) + " " + (13 - k.size()) + " " + (13 - h.size()) + " " + (13 - t.size()));
            else System.out.println("GRESKA");
        }
    }
}

class Card {

    private char suit;
    private int value;

    public Card (char suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public char getSuit(){
        return suit;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Card)) return false;

        Card card = (Card) other;
        if (card.getSuit() == this.getSuit() && card.getValue() == this.getValue()) return true;
        else return false;
    }

    @Override
    public String toString() {
        return suit + " " + value;
    }
}

class Suit {
    private HashSet<Card> suit;

    public Suit() {
        this.suit = new HashSet<Card>();
    }

    public boolean add (Card c) {
        return suit.add(c);
    }

    public int size() {
        return suit.size();
    }


}
